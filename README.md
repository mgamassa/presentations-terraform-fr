# TERRAFORM : Tutoriels FR

- 1. [TERRAFORM - 1. INTRODUCTION ET PRESENTATION](https://www.youtube.com/watch?v=_6AITxe5iVc)
- 2. [TERRAFORM - 2. INSTALLATION ET HELLO WORLD](https://www.youtube.com/watch?v=7gtzumVHZtE)
- 3. [TERRAFORM - 3. DEFINITIONS ET NOTIONS](https://www.youtube.com/watch?v=MHKVkNSAJWc)
- 4. [TERRAFORM - 4. VARIABLES ET LOCAL_EXEC : STRING, LISTE ET MAP](https://www.youtube.com/watch?v=LmHKEiZ1SeA)
- 5. [TERRAFORM - 5. VARIABLES : LA PRECEDENCE OU NIVEAUX DE DECLARATION (.TFVARS...)](https://www.youtube.com/watch?v=4l_y3D58_iE)
- 6. [TERRAFORM - 6. REMOTE_EXEC & FILE : COMMANDES VIA SSH - EX: NGINX](https://www.youtube.com/watch?v=vh58fGiGj-A)
- 7. [TERRAFORM - 7. REMOTE_EXEC : INSTALLATION DE DOCKER ET SA SOCKET](https://www.youtube.com/watch?v=huR8567SSQQ)
- 8. [TERRAFORM - 8. PROVIDER DOCKER : IMAGE ET CONTENEUR](https://www.youtube.com/watch?v=TidvqDcq2Qw)
- 9. [TERRAFORM - 9. LES MODULES : INTRODUCTION](https://www.youtube.com/watch?v=ahdsbN5-UYg)
- 10. [TERRAFORM - 10. LES MODULES : PREMIERS PAS](https://www.youtube.com/watch?v=WNxRZN-toCs)
- 11. [TERRAFORM - 11. LES MODULES : TERRAFORM APPLY -TARGET (DEPENDANCES)](https://www.youtube.com/watch?v=_oHoTgjEjdw)
- 12. [TERRAFORM - 12. PROVIDER DOCKER : UTILISATION DES NETWORKS](https://www.youtube.com/watch?v=DnwQE01J2no)
- 13. [TERRAFORM - 13. PROVIDER DOCKER : INSTALLER LES VOLUMES](https://www.youtube.com/watch?v=7KrEAIKa47c)
- 14. [TERRAFORM - 14. PROVIDER DOCKER : EXEMPLE WORDPRESS](https://www.youtube.com/watch?v=Wc88pYIoA54)

